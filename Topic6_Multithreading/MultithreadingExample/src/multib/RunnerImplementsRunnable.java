package multib;

/**
 * This will be a thread enabled class that implemented Runnable
 *
 * @since Dec 5, 2017
 * @author bjmaclean
 */
public class RunnerImplementsRunnable implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            
            System.out.println("Hello from runner2: " + i);
            
            MultiB.writeToFile("TEST from one thread");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                System.out.println("Interrupted exception caught");
            }
        }
    }

}
