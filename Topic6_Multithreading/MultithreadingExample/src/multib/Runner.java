package multib;

import java.util.Scanner;

/**
 *
 * @since Dec 5, 2017
 * @author bjmaclean
 */
public class Runner extends Thread {

    @Override
    public void run() {
        String userEntry = "";
        Scanner input = new Scanner(System.in);  //input object
        do {
            System.out.println("Enter your text");
            userEntry = input.nextLine();
        } while (!userEntry.equalsIgnoreCase("X"));
    }

}
