package multib;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @since Dec 5, 2017
 * @author bjmaclean
 */
public class MultiB {

    public static synchronized void doSomething() {
        System.out.println("doing something");
    }

    public static void writeToFile(String textToWrite) {
        String FILE_NAME = "/cis2232/journal.txt";
        try {
            //Create a file
            Files.createDirectories(Paths.get("/cis2232"));
        } catch (IOException ex) {
            System.out.println("io exception caught");
        }

        //Also write to the file when a new camper was added!!
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {
            fw = new FileWriter(FILE_NAME, true);
            bw = new BufferedWriter(fw);
            bw.write(textToWrite);
            System.out.println("Done");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }

                if (fw != null) {
                    fw.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {


        Runner runner1 = new Runner();

        Thread runner2 = new Thread(new RunnerImplementsRunnable());

        runner1.start();
        runner2.start();
        runner1.join();
        runner2.join();
        System.out.println("Finished main thread");
    }
}
