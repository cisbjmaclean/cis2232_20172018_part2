package info.hccis.bo;

import java.util.Calendar;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * This class will contain the unit tests associated with the Camper class.
 *
 * @author bjmaclean
 * @since 20171128
 */
public class CamperTest {

    private int currentYear;
    
    public CamperTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        //https://stackoverflow.com/questions/136419/get-integer-value-of-the-current-year-in-java
        currentYear = Calendar.getInstance().get(Calendar.YEAR);

        
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testThat7YearOldCampTypeNot1CostIs40() {

        //Make the dob seven years ago for this test
        String dob = (currentYear - 7) + "-03-06";

        Camper camper = new Camper(0, "Joe", "McLaine", dob, 4);
        assertEquals(40, camper.getCampDailyCost());
    }

    @Test
    public void testThat7YearOldCampType1CostIs50() {

        //Make the dob seven years ago for this test
        String dob = (currentYear - 7) + "-03-06";
        Camper camper = new Camper(0, "Joe", "McLaine", dob, 1);
        assertEquals(50, camper.getCampDailyCost());
    }

}
