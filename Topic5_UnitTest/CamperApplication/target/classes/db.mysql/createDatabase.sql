drop database canes;
create database canes;
use canes;

/*create a user in database*/
grant select, insert, update, delete on canes.* to 'cis2232_admin'@'localhost'
identified by 'Test1234';
flush privileges;

CREATE TABLE IF NOT EXISTS `Camper` (
`id` int(4) unsigned NOT NULL COMMENT 'Registration id',
`firstName` varchar(10) DEFAULT NULL,
`lastName` varchar(10) DEFAULT NULL,
`dob` varchar(10) DEFAULT NULL,
`campType` int(4) COMMENT 'Camp Type'
);


INSERT INTO `Camper` (`id`, `firstName`, `lastName`, `dob`,campType) VALUES
(1, 'Nick', 'Matheson', '12345',2);
INSERT INTO `Camper` (`id`, `firstName`, `lastName`, `dob`,campType) VALUES
(2, 'Audrey', 'Matheson', '12345',2);
INSERT INTO `Camper` (`id`, `firstName`, `lastName`, `dob`,campType) VALUES
(3, 'Patrick', 'Campbell', '12345',2);
INSERT INTO `Camper` (`id`, `firstName`, `lastName`, `dob`,campType) VALUES
(4, 'Nick', 'Smith', '12345',2);
