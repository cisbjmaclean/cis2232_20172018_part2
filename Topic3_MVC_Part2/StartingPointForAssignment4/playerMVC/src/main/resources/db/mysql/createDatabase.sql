--
-- Database: `cis2232_squash_registration`
--
drop database squash;
create database squash;
use squash;

/*create a user in database*/
grant select, insert, update, delete on squash.*
to 'cis2232_admin'@'localhost'
identified by 'Test1234';
flush privileges;

--
-- Table structure for table `player`
-- id table line used with permission from camper example
--

CREATE TABLE IF NOT EXISTS `player` (
`id` int(4) unsigned NOT NULL PRIMARY KEY COMMENT 'Registration id', 
`name` varchar(20) NOT NULL,
`parentName` varchar(20) NOT NULL,
`phoneNumber` varchar(20) NOT NULL,
`emailAddress` varchar(30) NOT NULL,
`amountPaid` float (5) NOT NULL
);