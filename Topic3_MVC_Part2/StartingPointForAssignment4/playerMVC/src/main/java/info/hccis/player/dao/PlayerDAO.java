package info.hccis.player.dao;

import info.hccis.player.model.jpa.Player;
import info.hccis.player.bo.PlayerValidationBO;
import info.hccis.player.dao.util.ConnectionUtils;
import info.hccis.player.dao.util.DbUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * This class will contain db functionality for working with Players
 *
 * @author bjmaclean
 * @since 20160929
 *
 * @update October 6, 2017 - refactored all references of Camper objects to
 * Player objects <fg>
 *
 * @update October 27, 2017 - refactored to update to a thymeleaf webpage <fgotell>
 */
public class PlayerDAO {

    private final static Logger LOGGER = Logger.getLogger(PlayerDAO.class.getName());

    public static Player select(int idIn) {

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        Player thePlayer = null;
        try {
            conn = ConnectionUtils.getConnection();

            // modified to support Player objects <Oct 9, 2017 Assignment 2 Fernand Gotell>
            sql = "SELECT * FROM player where id = ?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, idIn);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String parentName = rs.getString("parentName");
                String phoneNumber = rs.getString("phoneNumber");
                String emailAddress = rs.getString("emailAddress");
                float amountPaid = rs.getFloat("amountPaid");

                thePlayer = new Player(id, name, parentName, phoneNumber, emailAddress, amountPaid);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return thePlayer;
    }

    /**
     * refactored select method to delete
     * @param idIn 
     */
    public static void delete(int idIn) {

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();

            // modified to support Player objects <Oct 9, 2017 Assignment 2 Fernand Gotell>
            sql = "DELETE FROM player where id = ?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, idIn);
            ps.executeUpdate();

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
    }

    /**
     * Select all players
     *
     * @author bjmaclean
     * @since 20160929
     * @return
     */
    public static ArrayList<Player> selectAll() {

        ArrayList<Player> players = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM player";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                // modified to support Player objects <Oct 9, 2017 Assignment 2 Fernand Gotell>
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String parentName = rs.getString("parentName");
                String phoneNumber = rs.getString("phoneNumber");
                String emailAddress = rs.getString("emailAddress");
                float amountPaid = rs.getFloat("amountPaid");

                players.add(new Player(id, name, parentName, phoneNumber, emailAddress, amountPaid));
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        //Have to do some work here to get the players from the database.
        return players;
    }

    /**
     * This method will insert.
     *
     * @return
     * @author BJ
     * @since 20140615
     */
    public static synchronized Player update(Player player) throws Exception {
//        System.out.println("inserting player");
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;


        /*
         * Setup the sql to update or insert the row.
         */
        try {
            conn = ConnectionUtils.getConnection();

            //Check to see if player exists.
            if(player.getId() == null){
                player.setId(0);
            }
            if (player.getId() == 0) {

                sql = "SELECT max(id) from player";
                ps = conn.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();
                int max = 0;
                while (rs.next()) {
                    max = rs.getInt(1) + 1;
                }

                player.setId(max);

                // modified to support Player objects <Oct 9, 2017 Assignment 2 Fernand Gotell>
                sql = "INSERT INTO `player`(`id`, `name`, `parentName`, "
                        + "`phoneNumber`, `emailAddress`, `amountPaid`) "
                        + "VALUES (?,?,?,?,?,?)";
                ps = conn.prepareStatement(sql);

                ps.setInt(1, player.getId());
                ps.setString(2, player.getName());
                ps.setString(3, player.getParentName());
                ps.setString(4, player.getPhoneNumber());
                ps.setString(5, player.getEmailAddress());
                ps.setDouble(6, player.getAmountPaid());

            } else {

                // modified to support Player objects <Oct 9, 2017 Assignment 2 Fernand Gotell>
                sql = "UPDATE `Player` SET `id`=? ,`name`=?,`parentName`=?,"
                        + "`phoneNumber`=?, `emailAddress`=? ,`amountPaid`=?"
                        + " WHERE id = ?";
                
                ps = conn.prepareStatement(sql);
                ps.setInt(1, player.getId());
                ps.setString(2, player.getName());
                ps.setString(3, player.getParentName());
                ps.setString(4, player.getPhoneNumber());
                ps.setString(5, player.getEmailAddress());
                ps.setDouble(6, player.getAmountPaid());
                ps.setInt(7, player.getId());

            }
            /*
             Note executeUpdate() for update vs executeQuery for read only!!
             */

//            System.out.println(ps);
            ps.executeUpdate();

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;
        } finally {
            DbUtils.close(ps, conn);
        }
        return player;

    }

}
