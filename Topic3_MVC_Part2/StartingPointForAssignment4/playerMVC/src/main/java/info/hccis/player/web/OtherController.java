package info.hccis.player.web;

import info.hccis.player.model.jpa.Player;
import info.hccis.player.dao.PlayerDAO;
import info.hccis.player.model.jpa.User;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @since October 2017
 * 
 * @update October 27, 2017 - refactored to include a player object <fgotell>
 */

@Controller
public class OtherController {

    @RequestMapping("/")
    public String showHome(Model model, HttpSession session) {

        model.addAttribute("user", new User());
        session.removeAttribute("loggedInUser");
        //This will send the user to the welcome.html page.
        return "other/welcome";
    }

    @RequestMapping("/logout")
    public String logout(Model model, HttpSession session) {

        model.addAttribute("user", new User());
        session.removeAttribute("loggedInUser");
        //Give a message indicating that they have been logged out.
        model.addAttribute("message", "Successfully logged out");
        //This will send the user to the welcome.html page.
        return "other/welcome";
    }

    @RequestMapping("/authenticate")
    public String authenticate(Model model, @ModelAttribute("user") User user, HttpSession session) {

        session.removeAttribute("loggedInUser");

        if (user.getUsername().isEmpty()) {
            //failed validation
            model.addAttribute("message", "Put something in username");
            return "other/welcome";
        } else {
            //passed and send to the list page (player/list)
            //Get the players from the database
            session.setAttribute("loggedInUser", user);

            ArrayList<Player> players = PlayerDAO.selectAll();
            double totalFees = 0;
            for (Player playerTemp : players) {
                totalFees += playerTemp.getAmountPaid();
            }
            model.addAttribute("totalFees", totalFees);

            System.out.println("Fernand Gotell-found " + players.size() + " players.  Going to welcome page");
            model.addAttribute("players", players);
            return "player/list";
        }
    }

    @RequestMapping("/about")
    public String showAbout(Model model) {
        return "other/about";
    }

    @RequestMapping("/help")
    public String showHelp(Model model) {
        return "other/help";
    }

}
