package info.hccis.player.bo;

import info.hccis.player.model.jpa.Player;
import java.util.ArrayList;

/**
 *
 * @since Oct 18, 2017
 * @author bjmaclean
 * 
 * @update october 26, 2017 fernand gotell
 * 
 * @update October 27, 2017 - refactored to update to a thymeleaf webpage <fgotell>
 */
public class PlayerValidationBO {

    public static ArrayList<String> validatePlayer(Player player) {

        //Do some validation before trying the update.
        ArrayList<String> messages = new ArrayList();
        //First name required
        if (player.getName().isEmpty()) {
            messages.add("First name is required");
        }
        //Last name required
        if (player.getParentName().isEmpty()) {
            messages.add("Last name is required");
        }
        //Phone number is required
        if (player.getPhoneNumber().isEmpty()){
            messages.add("Phone number is is required");
        }
        //Email is required
        if (player.getEmailAddress().isEmpty()){
            messages.add("Email address is is required");
        }
        //Fee between 1 and 100 is required
        if (player.getAmountPaid()<0 || player.getAmountPaid()>50){
            messages.add("Enter fee between 0 and 50");
        }
        return messages;
    }

}
