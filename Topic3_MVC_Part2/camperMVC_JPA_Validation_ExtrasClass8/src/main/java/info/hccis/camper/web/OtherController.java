package info.hccis.camper.web;

import info.hccis.camper.dao.CamperDAO;
import info.hccis.camper.dao.CodeValueDAO;
import info.hccis.camper.data.springdatajpa.CodeValueRepository;
import info.hccis.camper.model.DatabaseConnection;
import info.hccis.camper.model.jpa.Camper;
import info.hccis.camper.model.jpa.CodeValue;
import info.hccis.camper.model.jpa.User;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OtherController {

    private final CodeValueRepository cvr;
    
    @Autowired
    public OtherController(CodeValueRepository cvr) {
        this.cvr = cvr;
    }
    
    @RequestMapping("/")
    public String showHome(Model model, HttpSession session) {

        model.addAttribute("user", new User());
        session.removeAttribute("loggedInUser");
        //This will send the user to the welcome.html page.
        return "other/welcome";
    }

    @RequestMapping("/logout")
    public String logout(Model model, HttpSession session) {

        model.addAttribute("user", new User());
        session.removeAttribute("loggedInUser");
        //Give a message indicating that they have been logged out.
        model.addAttribute("message", "Successfully logged out");
        //This will send the user to the welcome.html page.
        return "other/welcome";
    }
    
    @RequestMapping("/authenticate")
    public String authenticate(Model model, @ModelAttribute("user") User user, HttpSession session) {

        session.removeAttribute("loggedInUser");
        
        if (user.getUsername().isEmpty()) {
            //failed validation
            model.addAttribute("message","Put something in username");
            return "other/welcome";
        } else {
            //passed and send to the list page (camper/list)
            //Get the campers from the database
            session.setAttribute("loggedInUser", user);
            ArrayList<Camper> campers = CamperDAO.selectAll();
            System.out.println("BJM-found " + campers.size() + " campers.  Going to welcome page");
            model.addAttribute("campers", campers);
            
            //Load the camper types from CodeValue for use when adding a camper
            ArrayList<CodeValue> campTypes = (ArrayList<CodeValue>) cvr.findByCodeTypeId(2);
            System.out.println("BJM - added "+campTypes.size()+" to the session for camp types");
            session.setAttribute("CampTypes", campTypes);
            return "camper/list";
        }
    }

    @RequestMapping("/about")
    public String showAbout(Model model) {
        return "other/about";
    }

    @RequestMapping("/help")
    public String showHelp(Model model) {
        return "other/help";
    }

}
