package info.hccis.camper.web;

import info.hccis.camper.dao.CamperDAO;
import info.hccis.camper.data.springdatajpa.UserRepository;
import info.hccis.camper.model.jpa.Camper;
import info.hccis.camper.model.jpa.User;
import info.hccis.user.bo.UserBO;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OtherController {
    
    private final UserRepository ur;
    
    @Autowired
    public OtherController(UserRepository ur) {
        this.ur = ur;
    }
    
    @RequestMapping("/")
    public String showHome(Model model, HttpSession session) {
        
        model.addAttribute("user", new User());
        session.removeAttribute("loggedInUser");
        session.setAttribute("loggedInUser", new User());
        //This will send the user to the welcome.html page.
        return "other/welcome";
    }
    
    @RequestMapping("/logout")
    public String logout(Model model, HttpSession session) {
        
        model.addAttribute("user", new User());
        session.removeAttribute("loggedInUser");
        session.setAttribute("loggedInUser", new User());
        //Give a message indicating that they have been logged out.
        model.addAttribute("message", "Successfully logged out");
        //This will send the user to the welcome.html page.
        return "other/welcome";
    }
    
    @RequestMapping("/authenticate")
    public String authenticate(Model model, @ModelAttribute("user") User user, HttpSession session) {
        
        session.removeAttribute("loggedInUser");
        
        if (!UserBO.authenticate(user, ur)) {
            //failed validation
            session.setAttribute("loggedInUser", user);
            model.addAttribute("message", "Authentication failed");
            return "other/welcome";
        } else {
            //passed and send to the list page (camper/list)
            //Get the campers from the database
            
            session.setAttribute("loggedInUser", UserBO.getUserByUsername(user, ur));
            
            ArrayList<Camper> campers = CamperDAO.selectAll();
            System.out.println("BJM-found " + campers.size() + " campers.  Going to welcome page");
            model.addAttribute("campers", campers);
            return "camper/list";
        }
    }
    
    @RequestMapping("/about")
    public String showAbout(Model model) {
        return "other/about";
    }
    
    @RequestMapping("/help")
    public String showHelp(Model model) {
        return "other/help";
    }
    
}
