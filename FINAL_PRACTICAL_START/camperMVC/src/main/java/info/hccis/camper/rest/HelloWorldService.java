package info.hccis.camper.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
 
@Path("/HelloService")
public class HelloWorldService {
 
	@GET
	@Path("/hello/{param}")
	public Response getMsg(@PathParam("param") String msg) {
 
		String output = "Jersey say : " + msg;
                System.out.println("in Jersey hello web service");
		return Response.status(200).entity(output).build();
 
	}
 
}
