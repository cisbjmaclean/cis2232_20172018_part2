package info.hccis.camper.web;

import info.hccis.camper.bo.CamperValidationBO;
import info.hccis.camper.dao.CamperDAO;
import info.hccis.camper.data.springdatajpa.CamperRepository;
import info.hccis.camper.model.jpa.Camper;
import info.hccis.camper.model.jpa.User;
import info.hccis.camper.util.Utility;
import info.hccis.user.bo.UserBO;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CamperController {

    private final CamperRepository cr;

    @Autowired
    public CamperController(CamperRepository cr) {
        this.cr = cr;
    }

    @RequestMapping("/info")
    public String info(Model model, HttpSession session) {
        User current = (User) session.getAttribute("loggedInUser");
        if (current == null) {
            session.setAttribute("loggedInUser", new User());
        }
        
        //****************************************************************
        //Before you go to the info page, call the web service and 
        //check the temperature which comes back from the web service.
        //Set the message in the model before forwarding to the info view.
        //****************************************************************
        
        //****************************************************************
        //Call the method to get the json back from the rest call
        //****************************************************************
        String output = Utility.getJsonForRest("http://apidev.accuweather.com/currentconditions/v1/1327.json?language=en&apikey=hoArfRosT1215");
        System.out.println(output);

        //****************************************************************
        //get the temperature from the json string
        //****************************************************************
        int position = output.indexOf("Temperature\":{\"Metric\":{\"Value\":");
        int start = position + 32;
        int endPosition = output.indexOf(",", start+1);
        String temperature = output.substring(start, endPosition);
        System.out.println(temperature);
        float tempFloat = Float.parseFloat(temperature);
        
        //**********************************************************************
        //Otherwise use JSONArray and JSONObject classes to get the temperature
        //**********************************************************************

        JSONArray recs = new JSONArray(output);
        JSONObject rec = recs.getJSONObject(0);
        JSONObject tempRec = rec.getJSONObject("Temperature");
        JSONObject metricRec = tempRec.getJSONObject("Metric");
        float tempFloat2 = metricRec.getFloat("Value");
        System.out.println("Using JSONArray/JSONObject: "+tempFloat2);
        
        //****************************************************************
        //Show the message based on the temperature
        //****************************************************************
        String messageTemp = "Normal day";
        if(tempFloat > 20){
            messageTemp = "Bring your shorts it's hot";
        }
        if(tempFloat < 0){
            messageTemp = "Bring your coat, it's cold";
        }
        System.out.println(position);
        model.addAttribute("message", messageTemp);
        return "/other/info";
    }

    @RequestMapping("/camper/add")
    public String add(Model model, HttpSession session) {

        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if (!UserBO.verifyLoggedIn(user)) {
            model.addAttribute("user", new User());
            session.setAttribute("loggedInUser", new User());
            return "other/welcome";
        }

        Camper camper = new Camper();
        camper.setId(0);
        model.addAttribute("camper", camper);

        return "camper/add";
    }

    @RequestMapping("/camper/delete")
    public String delete(Model model, HttpSession session, HttpServletRequest request) {

        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if (!UserBO.verifyLoggedIn(user)) {
            model.addAttribute("user", new User());
            session.setAttribute("loggedInUser", new User());
            return "other/welcome";
        }

        String id = request.getParameter("id");
        //CamperDAO.delete(Integer.parseInt(id));
        cr.delete(Integer.parseInt(id));

        ArrayList<Camper> campers = (ArrayList<Camper>) cr.findAll(); //CamperDAO.selectAll();
        model.addAttribute("campers", campers);
        return "camper/list";
    }

    @RequestMapping("/camper/edit")
    public String edit(Model model, HttpSession session, HttpServletRequest request) {

        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if (!UserBO.verifyLoggedIn(user)) {
            model.addAttribute("user", new User());
            session.setAttribute("loggedInUser", new User());
            return "other/welcome";
        }

        String id = request.getParameter("id");
        Camper camper = cr.findOne(Integer.parseInt(id));
        model.addAttribute("camper", camper);

        return "camper/add";
    }

    @RequestMapping("/camper/addSubmit")
    public String addSubmit(@Valid @ModelAttribute("camper") Camper camper, BindingResult result, Model model, HttpSession session) {

        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if (!UserBO.verifyLoggedIn(user)) {
            model.addAttribute("user", new User());
            session.setAttribute("loggedInUser", new User());
            return "other/welcome";
        }

        boolean error = false;

        //Apply Spring Validation
        //If there are errors on the form then send the user back to the add page.
        if (result.hasErrors()) {
            System.out.println("BJM-There was an error validating the camper object");
            error = true;
        }

        ArrayList<String> errors = CamperValidationBO.validateCamper(camper);
        //If there is an error send them back to add page.

        if (!errors.isEmpty()) {
            error = true;
        }

        if (error) {
            model.addAttribute("messages", errors);
            return "/camper/add";
        }

        System.out.println("BJM - About to add " + camper + " to the database");
        try {
            cr.save(camper);
            //CamperDAO.update(camper);
            //Get the campers from the database
            ArrayList<Camper> campers = CamperDAO.selectAll();
            model.addAttribute("campers", campers);
        } catch (Exception ex) {
            System.out.println("BJM - There was an error adding camper to the database");
        }
        return "camper/list";
    }

    @RequestMapping("/camper/list")
    public String showHome(Model model, HttpSession session) {

        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if (!UserBO.verifyLoggedIn(user)) {
            model.addAttribute("user", new User());
            session.setAttribute("loggedInUser", new User());
            return "other/welcome";
        }

        //Get the campers from the database
        ArrayList<Camper> campers = CamperDAO.selectAll();
        System.out.println("BJM-found " + campers.size() + " campers.  Going to welcome page");
        model.addAttribute("campers", campers);

        //This will send the user to the welcome.html page.
        return "camper/list";
    }

}
