package info.hccis.camper.rest;

import com.google.gson.Gson;
import info.hccis.camper.dao.CamperDAO;
import info.hccis.camper.dao.UserDAO;
import info.hccis.camper.model.DatabaseConnection;
import info.hccis.camper.model.jpa.Camper;
import info.hccis.camper.model.jpa.User;
import java.util.ArrayList;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/UserService")
public class UserService {

    @GET
    @Path("/users")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers() {

        ArrayList<User> users = UserDAO.getUsers(new DatabaseConnection());
        Gson gson = new Gson();

        int statusCode = 200;
        if(users.isEmpty()){
            statusCode = 204;
        }
        
        String temp = "";
        temp = gson.toJson(users);

        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }


    
}
