package info.hccis.user.bo;

import info.hccis.camper.data.springdatajpa.UserRepository;
import info.hccis.camper.model.jpa.User;
import info.hccis.camper.util.Utility;
import java.util.ArrayList;

/**
 * This class will contain functionality related to working with user objects.
 *
 * @author bjmaclean
 * @since Nov 3, 2017
 */
public class UserBO {

    public static boolean authenticate(User user, UserRepository ur) {
        //Check that the user exists in the user table
        ArrayList<User> usersFromDB = (ArrayList<User>) ur.findByUsername(user.getUsername());
        boolean authenticated = false;
        boolean authenticatedUsername = false;

        if (usersFromDB.size() == 1) {
            authenticatedUsername = true;
        }

        //if they exist verify that the password hash matches the hashed pw in the db
        String hashedEnteredPassword = Utility.getHashPassword(user.getPassword());

        if (authenticatedUsername
                && usersFromDB.get(0).getPassword().equals(hashedEnteredPassword)) {
            authenticated = true;
        }

        return authenticated;

    }

    public static User getUserByUsername(User user, UserRepository ur) {

        ArrayList<User> usersFromDB = (ArrayList<User>) ur.findByUsername(user.getUsername());
        if (usersFromDB.size() > 0) {
            return usersFromDB.get(0);
        } else {
            return null;
        }

    }

    public static boolean verifyLoggedIn(User user) {

        try {
            if (user.getUserTypeCode() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

}
