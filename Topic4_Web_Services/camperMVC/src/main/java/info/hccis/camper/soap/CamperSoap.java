/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.camper.soap;

import info.hccis.camper.dao.CamperDAO;
import info.hccis.camper.model.jpa.Camper;
import java.util.ArrayList;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author bjmaclean
 */
@WebService(serviceName = "CamperSoap")
public class CamperSoap {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "getCamper")
    public Camper getCamper(@WebParam(name = "camperId") int camperId) {
        
        return CamperDAO.select(camperId);
        
    }
    
        /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "getCampers")
    public ArrayList<Camper> getCampers() {
        
        return CamperDAO.selectAll();
        
    }

}
