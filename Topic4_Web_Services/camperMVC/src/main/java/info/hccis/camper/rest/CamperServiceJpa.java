package info.hccis.camper.rest;

import com.google.gson.Gson;
import info.hccis.camper.data.springdatajpa.CamperRepository;
import info.hccis.camper.model.jpa.Camper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import static javax.ws.rs.HttpMethod.POST;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

//issues here...
//http://www.scriptscoop.net/t/ba4553fde8ca/spring-autowired-classes-are-null-in-jersey-rest.html

@Component
@Path("/CamperServiceJpa")
public class CamperServiceJpa {

    @Resource
    private final CamperRepository cr;

    
    public CamperServiceJpa(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.cr = applicationContext.getBean(CamperRepository .class);
    }
    

    
    @GET
    @Path("/campers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCampers() {

        //Get the campers from the database
        ArrayList<Camper> campers = (ArrayList<Camper>) cr.findAll(); //CamperDAO.selectAll();
        Gson gson = new Gson();

        int statusCode = 200;
        if(campers.isEmpty()){
            statusCode = 204;
        }
        
        String temp = "";
        temp = gson.toJson(campers);

        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }

    @GET
    @Path("/campers/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCamper(@PathParam("id") int id) {

        //Get the camper from the database
        Camper camper = cr.findOne(id); //CamperDAO.select(id);
        Gson gson = new Gson();

//        final ObjectMapper mapper = new ObjectMapper();
        String temp = "";
        temp = gson.toJson(camper);

        return Response.status(200).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }

 
    
/**
     * POST operation which will update a camper.
     * @param jsonIn 
     * @return response including the json representing the new camper.
     * @throws IOException 
     */
    
    @POST
    @Path("/campers")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateCamper(String jsonIn) throws IOException {

//        ObjectMapper mapper = new ObjectMapper();
        
        //JSON from String to Object
        Gson gson = new Gson();
        Camper camper = gson.fromJson(jsonIn, Camper.class);
        //Camper camper = mapper.readValue(jsonIn, Camper.class);
        camper = cr.save(camper);
        String temp = "";
            temp = gson.toJson(camper);
        
        return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }
    
    
    
}
