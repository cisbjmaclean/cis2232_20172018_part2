package info.hccis.camper.data.springdatajpa;

import info.hccis.camper.model.jpa.User;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

    List<User> findByUsername(String username);

}
