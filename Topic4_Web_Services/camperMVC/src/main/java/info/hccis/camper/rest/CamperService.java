package info.hccis.camper.rest;

import com.google.gson.Gson;
import info.hccis.camper.dao.CamperDAO;
import info.hccis.camper.model.jpa.Camper;
import java.util.ArrayList;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/CamperService")
public class CamperService {

    @GET
    @Path("/campers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCampers() {

        //Get the campers from the database
        ArrayList<Camper> campers = CamperDAO.selectAll();
        Gson gson = new Gson();

        int statusCode = 200;
        if(campers.isEmpty()){
            statusCode = 204;
        }
        
        String temp = "";
        temp = gson.toJson(campers);

        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }

    @GET
    @Path("/campers/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCamper(@PathParam("id") int id) {

        //Get the camper from the database
        Camper camper = CamperDAO.select(id);
        Gson gson = new Gson();

//        final ObjectMapper mapper = new ObjectMapper();
        String temp = "";
        temp = gson.toJson(camper);

        return Response.status(200).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }

    
}
